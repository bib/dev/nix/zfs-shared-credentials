# NixOS ZFS shared credentials

This NixOS module enables support for ZFS encryption key sharing among 
multiple users, using [GnuPG](https://en.wikipedia.org/wiki/GnuPG) and
[Shamir Secret Sharing](https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing).

## Features

 - Thresholded secret sharing using Shamir's secret sharing protocol
 - Remote multi-party unlocking using SSH and GnuPG
 - Support for boot-time key loading

## Motivations

 - ZFS only supports a single encryption key for each encryption root, 
   and managing this key can become tedious in multi-user installations.
 - Current remote unlock solutions already use SSH, so we can reuse their keys
 - Using GnuPG provides some level of key rotation (passphrases)

## Issues

 - Current status: **Work In Progress**
